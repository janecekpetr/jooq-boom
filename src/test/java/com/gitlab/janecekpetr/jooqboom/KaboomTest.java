package com.gitlab.janecekpetr.jooqboom;

import com.google.common.collect.ImmutableMap;
import org.junit.Test;

public class KaboomTest {
    
    private static final String[] VALUES_TO_INSERT = { "abc", "def" };
    
    private final Kaboom kaboom = new Kaboom();
    
    @Test
    public void testBindSimple() throws Exception {
        kaboom.insertPlease((batch, valueToInsert) -> batch.bind(valueToInsert), VALUES_TO_INSERT);
    }
    
    @Test
    public void testBindNamedParam() {
        kaboom.insertPlease((batch, valueToInsert) -> batch.bind(ImmutableMap.of("columnB", valueToInsert)), VALUES_TO_INSERT);
    }

}
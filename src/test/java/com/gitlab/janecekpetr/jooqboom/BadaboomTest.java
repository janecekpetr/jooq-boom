package com.gitlab.janecekpetr.jooqboom;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

public class BadaboomTest {
	
	@Test
	public void testBadaboom() throws Exception {
		assertThat(new Badaboom().selectPlease()).hasSize(3);
	}

}
package com.gitlab.janecekpetr.jooqboom;

import org.jooq.Record2;
import org.junit.Test;

import static com.gitlab.janecekpetr.model.Tables.BANG;
import static org.assertj.core.api.Assertions.assertThat;

public class KabangTest {

    private final Kabang kabang = new Kabang();

    @Test
    public void testSelectAggregatedRowValueExpressions() throws Exception {
        Record2<Integer, Integer>[] fooBars = kabang.selectPlease();
        assertThat(fooBars)
            .allMatch(barQux -> barQux.get(BANG.FOO) == 1)
            .allMatch(barQux -> barQux.get(BANG.BAR) == 2);
    }

}
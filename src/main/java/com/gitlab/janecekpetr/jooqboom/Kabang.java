package com.gitlab.janecekpetr.jooqboom;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.jooq.Configuration;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.Record2;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;
import org.jooq.impl.DefaultConfiguration;
import org.postgresql.ds.PGSimpleDataSource;

import static com.gitlab.janecekpetr.model.Tables.BANG;
import static org.jooq.impl.DSL.arrayAgg;
import static org.jooq.impl.DSL.row;
import static org.jooq.impl.DSL.rowField;

/**
 * @see <a href="https://github.com/jOOQ/jOOQ/issues/7691">jOOQ issue #7691</a>
 */
public class Kabang {

    private static final Field<Record2<Integer, Integer>[]> BANG_AGGREGATION = arrayAgg(
            rowField(row(BANG.FOO, BANG.BAR))
        ).as("foobar");

    public Record2<Integer, Integer>[] selectPlease() {
        try (HikariDataSource dataSource = createDataSource()) {
            Configuration jooqConfig = new DefaultConfiguration()
                .set(dataSource)
                .set(SQLDialect.POSTGRES_9_5);

            DSLContext context = DSL.using(jooqConfig);

            context.insertInto(BANG)
                .columns(BANG.FOO, BANG.BAR)
                .values(1, 2)
                .execute();

            return selectAggregatedRowValueExpressions(context);
        }
    }

    private static HikariDataSource createDataSource() {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setDataSourceClassName(PGSimpleDataSource.class.getName());

        hikariConfig.addDataSourceProperty("serverName", "localhost");
        hikariConfig.addDataSourceProperty("portNumber", "5432");
        hikariConfig.addDataSourceProperty("databaseName", "jooq_boom");
        hikariConfig.setUsername("postgres");
        hikariConfig.setPassword("postgres");

        return new HikariDataSource(hikariConfig);
    }

    private static Record2<Integer, Integer>[] selectAggregatedRowValueExpressions(DSLContext context) {
        return context.select(BANG_AGGREGATION)
            .from(BANG)
            .fetchSingle(BANG_AGGREGATION);
    }

}
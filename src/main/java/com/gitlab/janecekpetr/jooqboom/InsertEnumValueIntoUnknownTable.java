package com.gitlab.janecekpetr.jooqboom;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.jooq.Configuration;
import org.jooq.DSLContext;
import org.jooq.EnumType;
import org.jooq.SQLDialect;
import org.jooq.Schema;
import org.jooq.impl.DSL;
import org.jooq.impl.DefaultConfiguration;
import org.postgresql.ds.PGSimpleDataSource;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.schema;
import static org.jooq.impl.DSL.table;

/**
 * Is there any way to insert into an "unknown" (not generated) table with an enum column using jOOQ?
 *
 * @see <a href="https://stackoverflow.com/questions/52814083/">my SO question</a>
 */
public class InsertEnumValueIntoUnknownTable {

    private enum Direction implements EnumType {
        NORTH, EAST, SOUTH, WEST;

        @Override
        public Schema getSchema() {
            return schema(DSL.name("public"));
        }

        @Override
        public String getName() {
            return "direction";
        }

        @Override
        public String getLiteral() {
            return this.name().toLowerCase();
        }
    }

    public void insertEnum() {
        try (HikariDataSource dataSource = createDataSource()) {
            Configuration jooqConfig = new DefaultConfiguration()
                .set(dataSource)
                .set(SQLDialect.POSTGRES_9_5);

            DSLContext context = DSL.using(jooqConfig);

            context.insertInto(table("enum_table"))
                .columns(field("direction", Direction.class))
                .values(Direction.NORTH)
                .execute();
        }
    }

    private static HikariDataSource createDataSource() {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setDataSourceClassName(PGSimpleDataSource.class.getName());

        hikariConfig.addDataSourceProperty("serverName", "localhost");
        hikariConfig.addDataSourceProperty("portNumber", "5432");
        hikariConfig.addDataSourceProperty("databaseName", "jooq_boom");
        hikariConfig.setUsername("postgres");
        hikariConfig.setPassword("postgres");

        return new HikariDataSource(hikariConfig);
    }

}
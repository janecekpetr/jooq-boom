package com.gitlab.janecekpetr.jooqboom;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.jooq.Configuration;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;
import org.jooq.impl.DefaultConfiguration;
import org.postgresql.ds.PGSimpleDataSource;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.table;

/**
 * @see <a href="https://github.com/jOOQ/jOOQ/issues/7841">jOOQ issue #7841</a>
 */
public class Badaboom {

    public Result<Record> selectPlease() {
        try (HikariDataSource dataSource = createDataSource()) {
            Configuration jooqConfig = new DefaultConfiguration()
                .set(dataSource)
                .set(SQLDialect.POSTGRES_9_5);

            DSLContext context = DSL.using(jooqConfig);

			return context.select(DSL.asterisk(), field("foo"))
				.from(table("bang"))
				.fetch();
        }
    }

    private static HikariDataSource createDataSource() {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setDataSourceClassName(PGSimpleDataSource.class.getName());

        hikariConfig.addDataSourceProperty("serverName", "localhost");
        hikariConfig.addDataSourceProperty("portNumber", "5432");
        hikariConfig.addDataSourceProperty("databaseName", "jooq_boom");
        hikariConfig.setUsername("postgres");
        hikariConfig.setPassword("postgres");

        return new HikariDataSource(hikariConfig);
    }

}
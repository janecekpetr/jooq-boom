package com.gitlab.janecekpetr.jooqboom;

import com.gitlab.janecekpetr.model.tables.records.ARecord;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.jooq.BatchBindStep;
import org.jooq.Configuration;
import org.jooq.DSLContext;
import org.jooq.Insert;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;
import org.jooq.impl.DefaultConfiguration;
import org.postgresql.ds.PGSimpleDataSource;

import java.util.function.BiConsumer;

import static com.gitlab.janecekpetr.model.tables.A.A;
import static org.jooq.impl.DSL.inline;
import static org.jooq.impl.DSL.insertInto;
import static org.jooq.impl.DSL.param;

/**
 * @see <a href="https://github.com/jOOQ/jOOQ/issues/7319">jOOQ issue #7319</a>
 */
public class Kaboom {

    public void insertPlease(BiConsumer<BatchBindStep, String> batchBinder, String... valuesToInsert) {
        try (HikariDataSource dataSource = createDataSource()) {
            Configuration jooqConfig = new DefaultConfiguration()
                .set(dataSource)
                .set(SQLDialect.POSTGRES_9_5);

            DSLContext context = DSL.using(jooqConfig);

            Insert<ARecord> insert = insertInto(A)
                .columns(A.COLUMN_B)
                .values(param("columnB", A.COLUMN_B))
                .onDuplicateKeyUpdate()
                    .set(A.COLUMN_B, inline("nope"));

            BatchBindStep batch = context.batch(insert);
            for (String valueToInsert : valuesToInsert) {
                batchBinder.accept(batch, valueToInsert);
            }
            batch.execute();
        }
    }

    private static HikariDataSource createDataSource() {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setDataSourceClassName(PGSimpleDataSource.class.getName());

        hikariConfig.addDataSourceProperty("serverName", "localhost");
        hikariConfig.addDataSourceProperty("portNumber", "5432");
        hikariConfig.addDataSourceProperty("databaseName", "jooq_boom");
        hikariConfig.setUsername("postgres");
        hikariConfig.setPassword("postgres");

        return new HikariDataSource(hikariConfig);
    }

}